import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import ServiceFactory from '../services/ServiceFactory';

class ShowExpenses extends Component {
  constructor (props) {
    super(props);
    this.state = {
      expenses: [],
      sources: []
    };
    this.getSourceName = this.getSourceName.bind(this);
  }

  componentDidMount () {
    ServiceFactory.getService('expenseService')
      .index()
      .then((data) => this.setState({expenses: data}));
    ServiceFactory.getService('sourceService')
      .index()
      .then((sources) => this.setState({sources: sources}));
  }

  getSourceName (sourceId) {
    let name = this.state.sources.map((el, index) => {
      if (el.id === sourceId) {
        return el.name;
      }
    });
    return name;
  }

  render () {
    return (
      <table className='table table-striped table-dark'>
        <thead>
          <tr>
            <th scope='col'>id</th>
            <th scope='col'>Source</th>
            <th scope='col'>Name</th>
            <th scope='col'>Price(in ₴)</th>
            <th scope='col' className='EditStyle'>Edit</th>
          </tr>
        </thead>
        <tbody>
          {this.state.expenses.map((el, index) => {
            let sourceName = this.getSourceName(el.source_id);
            return (
              <tr key={index}>
                <td>{ el.id }</td>
                <td>{ sourceName }</td>
                <td>{ el.name }</td>
                <td>{ el.price }</td>
                <td className='tdEdit'>
                  <Link to={`/expenses/${el.id}/edit`} className='btn btn-primary'>Edit</Link>
                  <Link to={`/expenses/${el.id}/delete`} className='btn btn-danger'>Delete</Link>
                </td>
              </tr>);
          })}
        </tbody>
      </table>
    )
  }
}

export default ShowExpenses;
