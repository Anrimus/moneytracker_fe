import React, {Component} from 'react';
import ServiceFactory from '../services/ServiceFactory';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class DeleteExpense extends Component {
  componentDidMount () {
    ServiceFactory.getService('expenseService').delete(this.props.match.params.expenseId);
  }

  render () {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-8 col-md-offset-2'>
            <div className='panel panel-default'>
              <div className='panel-heading'>Delete Expense</div>
                <h1>EXPENSE WAS DELETED!</h1>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DeleteExpense;
