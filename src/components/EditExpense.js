import React, {Component} from 'react';
import ServiceFactory from '../services/ServiceFactory';
import { Redirect } from 'react-router-dom';

class EditExpense extends Component {
  constructor (props) {
    super(props);
    this.state = {
      sources: [],
      name: '',
      source_id: '',
      expense: [],
      price: 0
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount () {
    ServiceFactory.getService('expenseService').getExpense(this.props.match.params.expenseId).then((expense) => this.setState({
      expense: expense,
      source_id: expense.source_id,
      name: expense.name,
      price: expense.price
    }));
    ServiceFactory.getService('sourceService').index().then((sources) => this.setState({
      sources: sources
    }));
  }

  handleChange (event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit (event) {
    event.preventDefault();
    ServiceFactory.getService('expenseService').update(this.state.expense.id, this.state.source_id, this.state.name, this.state.price).then((response) => {
      console.log(response);
      if (response.statusText === 'OK') {
        <Redirect to="/" />
      }
    }).catch((params) => {
      console.log(params);
    });
  }

  render () {
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md-8 col-md-offset-2'>
            <div className='panel panel-default'>
              <div className='panel-heading'>Edit Expense</div>

              <div className='panel-body'>
                <form className='form-horizontal' method='POST' onSubmit={this.handleSubmit}>
                  <label htmlFor='name' className='col-md-4 control-label'>Name</label>

                  <div className='col-md-6'>
                    <input id='name' type='text' className='form-control' name='name' value={this.state.name} onChange={this.handleChange} required autoFocus />

                  </div>
                  <label htmlFor='source_id' className='col-md-4 control-label'>Source</label>

                  <div className='col-md-6'>
                    <select className='form-control' id='source_id' name='source_id' onChange={this.handleChange}>
                      {this.state.sources.map((el, index) => {
                        let option = <option key={index} value={el.id}>{el.name}</option>;
                        this.state.source_id === el.id ? option = <option key={index} defaultValue value={el.id}>{el.name}</option> : '';
                        return (option);
                      })}
                    </select>
                  </div>

                  <label htmlFor='price' className='col-md-4 control-label'>Price(in ₴)</label>

                  <div className='col-md-6'>
                    <input id='price' type='text' className='form-control' name='price' value={this.state.price} onChange={this.handleChange} required />
                  </div>

                  <div className='form-group'>
                    <div className='col-md-6 col-md-offset-4'>
                      <button type='submit' className='btn btn-primary'>
                                        Save
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default EditExpense;
