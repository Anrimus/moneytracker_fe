import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import EditExpense from './components/EditExpense';
import DeleteExpense from './components/DeleteExpense';
import ShowExpenses from './components/ShowExpenses';
import ExpenseService from './services/ExpenseService';
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {
  constructor () {
    super();
    this.state = {
      ExpenseService: new ExpenseService()
    };
  }

  render () {
    return (
      <div className='App'>
        <Router>
          <div>
            <Route path={`/expenses/:expenseId/edit`} component={EditExpense} />
            <Route exact path={`/`} component={ShowExpenses} />
            <Route exact path={`/expenses/:expenseId`} component={DeleteExpense} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
