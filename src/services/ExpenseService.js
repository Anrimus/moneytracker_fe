import BasicService from './BasicService';
import axios from 'axios';

export default class ExpenseService extends BasicService {
  index () {
    return this.fetch('http://localhost:8000/expenses').then(res => res.json());
  }
  getExpense (id) {
    return this.fetch(`http://localhost:8000/expenses/${id}`).then(res => res.json());
  }

  update (id, sourceId, name, price) {
    return axios.post('http://localhost:8000/expenses/' + id, {
      'id': id,
      'source_id': sourceId,
      'name': name,
      'price': price
    }, {headers: {'Content-Type': 'application/json'}});
  }

  delete (id) {
    return axios.delete('http://localhost:8000/expenses/' + id, {
      'id': id
    }, {headers: {'Content-Type': 'application/json'}});
  }
}
