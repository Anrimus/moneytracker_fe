import ExpenseService from './ExpenseService';
import SourceService from './SourceService';

export default class ServiceFactory {
  static get availableServices () {
    return {
      'expenseService': ExpenseService,
      'sourceService': SourceService
    };
  }
  static getService (name) {
    if (ServiceFactory.availableServices[name]) {
      if (!ServiceFactory.services[name]) {
        ServiceFactory.services[name] = new (ServiceFactory.availableServices[name])();
      }

      return ServiceFactory.services[name];
    } else {
      throw Error('no such service!');
    }
  }
}

ServiceFactory.services = {};
